#ifndef PGM_HPP
#define PGM_HPP

#include "imagem.hpp"

class PGM : public Imagem{
private:
	int deslocador;
	string mensagem;

public:
	PGM(string caminho);
	~PGM();
	void savePGM();
	string getMensagem();
};

#endif